import Config

# Configure your database
config :plasma, Plasma.Repo,
  username: "postgres",
  password: "postgres",
  database: "plasma_dev",
  hostname: "localhost",
  show_sensitive_data_on_connection_error: true,
  pool_size: 10

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"
