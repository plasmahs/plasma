defmodule Plasma.Factory do
  use ExMachina.Ecto, repo: Plasma.Repo

  def user_factory() do
    %Plasma.Repo.Model.User{
      mx_user_id: sequence(:user_id, &"@user#{&1}:localhost")
    }
  end

  def account_factory() do
    %Plasma.Repo.Model.Account{
      kind: "user",
      user: build(:user),
      password: "password",
      activated: true
    }
    |> Map.merge(Argon2.add_hash("password"))
  end

  def device_factory() do
    %Plasma.Repo.Model.Device{
      mx_device_id: sequence(:device_id, &"device_#{&1}"),
      user: build(:user)
    }
  end

  def access_token_factory() do
    %Plasma.Repo.Model.AccessToken{
      value: sequence(:value, &"access_token_value_#{&1}")
    }
  end

  def room_factory() do
    %Plasma.Repo.Model.Room{
      mx_room_id: sequence(:room_id, &"!room#{&1}:localhost")
    }
  end

  def account_data_factory() do
    %Plasma.Repo.Model.AccountData{
      type: "some_type",
      content: "{}"
    }
  end
end
