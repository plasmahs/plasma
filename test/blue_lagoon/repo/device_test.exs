defmodule Plasma.Repo.DeviceTest do
  use ExUnit.Case, async: true
  alias Plasma.Repo.Model.Device

  test "create device changeset without mx_device_id generates random ID" do
    device = Device.create_changeset(%Device{})
    assert %{mx_device_id: mx_device_id} = device.changes
    assert not is_nil(mx_device_id)
    assert String.trim(mx_device_id) != ""
  end
end
