defmodule Plasma.Client.AccountDataTest do
  use ExUnit.Case, async: false
  use Plug.Test
  use Plasma.DataCase
  import Plasma.Factory

  test "PUT /_matrix/client/v3/user/{userId}/account_data/{type}" do
    user = insert(:user)
    insert(:account, user: user)
    device = insert(:device, user: user)
    token = insert(:access_token, device: device)

    conn =
      conn(
        :put,
        "/_matrix/client/v3/user/#{URI.encode(user.mx_user_id)}/account_data/some_type",
        Jason.encode!(%{"some_key" => "some_value"})
      )
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer " <> token.value)

    conn =
      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )

    assert conn.status == 200
  end

  test "PUT /_matrix/client/v3/user/{userId}/rooms/{roomId}/account_data/{type}" do
    user = insert(:user)
    insert(:account, user: user)
    device = insert(:device, user: user)
    token = insert(:access_token, device: device)
    room = insert(:room)

    conn =
      conn(
        :put,
        "/_matrix/client/v3/user/#{URI.encode(user.mx_user_id)}/rooms/#{URI.encode(room.mx_room_id)}/account_data/some_type",
        Jason.encode!(%{"some_key" => "some_value"})
      )
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer " <> token.value)

    conn =
      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )

    assert conn.status == 200
  end

  test "GET /_matrix/client/v3/user/{userId}/account_data/{type}" do
    user = insert(:user)
    account = insert(:account, user: user)
    device = insert(:device, user: user)
    token = insert(:access_token, device: device)

    insert(:account_data, account: account, type: "test_type", content: %{"test" => "content"})

    conn =
      conn(
        :get,
        "/_matrix/client/v3/user/#{URI.encode(user.mx_user_id)}/account_data/test_type"
      )
      |> put_req_header("authorization", "Bearer " <> token.value)

    conn =
      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )

    assert conn.status == 200
    assert {:ok, response} = Jason.decode(conn.resp_body)
    assert %{"test" => "content"} == response
  end

  test "GET /_matrix/client/v3/user/{userId}/rooms/{roomId}//account_data/{type}" do
    user = insert(:user)
    account = insert(:account, user: user)
    device = insert(:device, user: user)
    token = insert(:access_token, device: device)
    room = insert(:room)

    insert(:account_data,
      account: account,
      type: "test_type",
      room: room,
      content: %{"test" => "content"}
    )

    conn =
      conn(
        :get,
        "/_matrix/client/v3/user/#{URI.encode(user.mx_user_id)}/rooms/#{URI.encode(room.mx_room_id)}/account_data/test_type"
      )
      |> put_req_header("authorization", "Bearer " <> token.value)

    conn =
      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )

    assert conn.status == 200
    assert {:ok, response} = Jason.decode(conn.resp_body)
    assert %{"test" => "content"} == response
  end
end
