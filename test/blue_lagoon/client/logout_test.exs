defmodule Plasma.Client.LogoutTest do
  use ExUnit.Case, async: false
  use Plug.Test
  use Plasma.DataCase
  import Plasma.Factory

  test "POST /_matrix/client/r0/logout" do
    user = insert(:user)
    insert(:account, user: user)
    device = insert(:device, user: user)
    token = insert(:access_token, device: device)

    conn =
      conn(:post, "/_matrix/client/r0/logout")
      |> put_req_header("authorization", "Bearer " <> token.value)

    conn =
      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )

    assert conn.status == 200
  end

  test "POST /_matrix/client/r0/logout/all" do
    user = insert(:user)
    insert(:account, user: user)
    device1 = insert(:device, user: user)
    token = insert(:access_token, device: device1)
    _device2 = insert(:device, user: user)

    conn =
      conn(:post, "/_matrix/client/r0/logout/all")
      |> put_req_header("authorization", "Bearer " <> token.value)

    conn =
      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )

    assert conn.status == 200
  end
end
