defmodule Plasma.Client.LoginTest do
  use ExUnit.Case, async: false
  use Plug.Test
  use Plasma.DataCase
  import Plasma.Factory

  test "POST /_matrix/client/r0/login with valid user and password" do
    user = insert(:user)
    account = insert(:account, user: user)
    insert(:device, user: user)

    body =
      Jason.encode!(%{
        "type" => "m.login.password",
        "identifier" => %{"type" => "m.id.user", "user" => user.mx_user_id},
        "password" => account.password
      })

    conn =
      conn(:post, "/_matrix/client/r0/login", body)
      |> put_req_header("content-type", "application/json")

    conn =
      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )

    Oban.drain_queue(queue: :default)

    assert conn.status == 200
    assert {:ok, response} = Jason.decode(conn.resp_body)

    assert %{"access_token" => access_token, "device_id" => device_id, "user_id" => user_id} =
             response

    assert String.trim(access_token) != ""
    assert String.trim(device_id) != ""
    assert user_id == user.mx_user_id
  end

  test "POST /_matrix/client/r0/login with valid user, password and device_id" do
    user = insert(:user)
    account = insert(:account, user: user)
    device = insert(:device, user: user)

    body =
      Jason.encode!(%{
        "type" => "m.login.password",
        "identifier" => %{"type" => "m.id.user", "user" => user.mx_user_id},
        "password" => account.password,
        "device_id" => device.mx_device_id
      })

    conn =
      conn(:post, "/_matrix/client/r0/login", body)
      |> put_req_header("content-type", "application/json")

    conn =
      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )

    Oban.drain_queue(queue: :default)

    assert conn.status == 200
    assert {:ok, response} = Jason.decode(conn.resp_body)

    assert %{"access_token" => access_token, "device_id" => device_id, "user_id" => user_id} =
             response

    assert String.trim(access_token) != ""
    assert device_id == device.mx_device_id
    assert user_id == user.mx_user_id
  end

  test "POST /_matrix/client/r0/login with wrong password" do
    user = build(:user)
    build(:account, user: user)
    insert(:device, user: user)

    body =
      Jason.encode!(%{
        "type" => "m.login.password",
        "identifier" => %{"type" => "m.id.user", "user" => user.mx_user_id},
        "password" => "wrong password"
      })

    assert_raise Plug.Conn.WrapperError, fn ->
      conn =
        conn(:post, "/_matrix/client/r0/login", body)
        |> put_req_header("content-type", "application/json")

      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )
    end
  end
end
