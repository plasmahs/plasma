defmodule Plasma.Client.VersionTest do
  use ExUnit.Case, async: true
  use Plug.Test

  test "GET /_matrix/client/versions" do
    conn = conn(:get, "/_matrix/client/versions")

    conn =
      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )

    assert conn.status == 200
    assert {:ok, response} = Jason.decode(conn.resp_body)
    assert %{"versions" => versions} = response
    assert is_list(versions)
  end
end
