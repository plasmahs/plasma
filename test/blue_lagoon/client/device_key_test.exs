defmodule Plasma.Client.DeviceKeyTest do
  use ExUnit.Case, async: false
  use Plug.Test
  use Plasma.DataCase
  import Plasma.Factory

  @pubkey <<71, 147, 206, 109, 245, 113, 97, 55, 178, 43, 26, 108, 229, 171, 63, 255, 34, 207, 25,
            248, 87, 186, 60, 92, 190, 159, 181, 171, 221, 171, 155, 10>>
  @privkey <<156, 1, 81, 214, 0, 61, 60, 255, 213, 105, 136, 90, 39, 0, 100, 85, 145, 159, 186,
             15, 201, 88, 249, 35, 56, 156, 174, 210, 170, 94, 10, 128>>

  test "POST /_matrix/client/v3/keys/upload" do
    user = insert(:user, mx_user_id: "@alice:example.org")
    insert(:account, user: user)
    device = insert(:device, user: user, mx_device_id: "ADEVICE")
    token = insert(:access_token, device: device)

    signing_key = %Polyjuice.Util.Ed25519.SigningKey{key: @privkey, id: "ADEVICE"}

    {:ok, device_keys} =
      %{
        "algorithms" => ["m.olm.v1.curve25519-aes-sha2", "m.megolm.v1.aes-sha2"],
        "device_id" => "ADEVICE",
        "keys" => %{
          "ed25519:ADEVICE" => Base.encode64(@pubkey, padding: false)
        },
        "user_id" => "@alice:example.org"
      }
      |> Polyjuice.Util.JSON.sign("@alice:example.org", signing_key)

    conn =
      conn(
        :post,
        "/_matrix/client/r0/keys/upload",
        Jason.encode!(%{
          "device_keys" => device_keys,
          "one_time_keys" => %{"dummy:AAAAA" => "thisIsAKey"}
        })
      )
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer " <> token.value)

    conn =
      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )

    assert conn.status == 200
    assert {:ok, _} = Jason.decode(conn.resp_body)
  end

  test "POST /_matrix/client/v3/keys/query with one user, no devices" do
    alice = insert(:user, mx_user_id: "@alice:example.org")
    insert(:account, user: alice)
    alice_device = insert(:device, user: alice, mx_device_id: "ADEVICE")
    token = insert(:access_token, device: alice_device)

    signing_key = %Polyjuice.Util.Ed25519.SigningKey{key: @privkey, id: "ADEVICE"}

    {:ok, device_keys} =
      %{
        "algorithms" => ["m.olm.v1.curve25519-aes-sha2", "m.megolm.v1.aes-sha2"],
        "device_id" => "ADEVICE",
        "keys" => %{
          "ed25519:ADEVICE" => Base.encode64(@pubkey, padding: false)
        },
        "user_id" => "@alice:example.org"
      }
      |> Polyjuice.Util.JSON.sign("@alice:example.org", signing_key)

    conn =
      conn(
        :post,
        "/_matrix/client/r0/keys/upload",
        Jason.encode!(%{
          "device_keys" => device_keys
        })
      )
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer " <> token.value)

    conn =
      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )

    assert conn.status == 200

    conn =
      conn(
        :post,
        "/_matrix/client/r0/keys/query",
        Jason.encode!(%{
          "device_keys" => %{"@alice:example.org": []}
        })
      )
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer " <> token.value)

    conn =
      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )

    assert conn.status == 200
    assert {:ok, body} = Jason.decode(conn.resp_body)
    assert %{"device_keys" => %{"@alice:example.org" => %{"ADEVICE" => _key}}} = body
  end

  test "POST /_matrix/client/v3/keys/query with one user, one device" do
    alice = insert(:user, mx_user_id: "@alice:example.org")
    insert(:account, user: alice)
    alice_device = insert(:device, user: alice, mx_device_id: "ADEVICE")
    token = insert(:access_token, device: alice_device)

    signing_key = %Polyjuice.Util.Ed25519.SigningKey{key: @privkey, id: "ADEVICE"}

    {:ok, device_keys} =
      %{
        "algorithms" => ["m.olm.v1.curve25519-aes-sha2", "m.megolm.v1.aes-sha2"],
        "device_id" => "ADEVICE",
        "keys" => %{
          "ed25519:ADEVICE" => Base.encode64(@pubkey, padding: false)
        },
        "user_id" => "@alice:example.org"
      }
      |> Polyjuice.Util.JSON.sign("@alice:example.org", signing_key)

    conn =
      conn(
        :post,
        "/_matrix/client/r0/keys/upload",
        Jason.encode!(%{
          "device_keys" => device_keys
        })
      )
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer " <> token.value)

    conn =
      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )

    assert conn.status == 200

    conn =
      conn(
        :post,
        "/_matrix/client/r0/keys/query",
        Jason.encode!(%{
          "device_keys" => %{"@alice:example.org": ["ADEVICE"]}
        })
      )
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer " <> token.value)

    conn =
      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )

    assert conn.status == 200
    assert {:ok, body} = Jason.decode(conn.resp_body)
    assert %{"device_keys" => %{"@alice:example.org" => %{"ADEVICE" => _key}}} = body
  end

  test "POST /_matrix/client/v3/keys/query with one user, two device" do
    alice = insert(:user, mx_user_id: "@alice:example.org")
    insert(:account, user: alice)
    bdevice = insert(:device, user: alice, mx_device_id: "BDEVICE")
    alice_device = insert(:device, user: alice, mx_device_id: "ADEVICE")
    token = insert(:access_token, device: alice_device)
    tokenb = insert(:access_token, device: bdevice)

    signing_key = %Polyjuice.Util.Ed25519.SigningKey{key: @privkey, id: "ADEVICE"}

    {:ok, device_keys} =
      %{
        "algorithms" => ["m.olm.v1.curve25519-aes-sha2", "m.megolm.v1.aes-sha2"],
        "device_id" => "ADEVICE",
        "keys" => %{
          "ed25519:ADEVICE" => Base.encode64(@pubkey, padding: false)
        },
        "user_id" => "@alice:example.org"
      }
      |> Polyjuice.Util.JSON.sign("@alice:example.org", signing_key)

    conn =
      conn(
        :post,
        "/_matrix/client/r0/keys/upload",
        Jason.encode!(%{
          "device_keys" => device_keys
        })
      )
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer " <> token.value)

    conn =
      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )

    assert conn.status == 200

    signing_key = %Polyjuice.Util.Ed25519.SigningKey{key: @privkey, id: "BDEVICE"}

    {:ok, device_keys} =
      %{
        "algorithms" => ["m.olm.v1.curve25519-aes-sha2", "m.megolm.v1.aes-sha2"],
        "device_id" => "BDEVICE",
        "keys" => %{
          "ed25519:BDEVICE" => Base.encode64(@pubkey, padding: false)
        },
        "user_id" => "@alice:example.org"
      }
      |> Polyjuice.Util.JSON.sign("@alice:example.org", signing_key)

    conn =
      conn(
        :post,
        "/_matrix/client/r0/keys/upload",
        Jason.encode!(%{
          "device_keys" => device_keys
        })
      )
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer " <> tokenb.value)

    conn =
      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )

    assert conn.status == 200

    conn =
      conn(
        :post,
        "/_matrix/client/r0/keys/query",
        Jason.encode!(%{
          "device_keys" => %{"@alice:example.org": ["BDEVICE"]}
        })
      )
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer " <> token.value)

    conn =
      Plasma.Plug.Matrix.call(
        conn,
        server: %Plasma.Backend{}
      )

    assert conn.status == 200
    assert {:ok, body} = Jason.decode(conn.resp_body)
    assert %{"device_keys" => %{"@alice:example.org" => %{"BDEVICE" => _key}}} = body
  end
end
