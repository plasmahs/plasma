defmodule Plasma.Services.AuthTest do
  use Plasma.DataCase, async: false
  import Plasma.Factory
  alias Plasma.Repo.Model.Device
  alias Plasma.Repo.Model.User
  alias Plasma.Repo.Model.Account
  alias Plasma.Repo.Model.AccessToken

  test "get_credentials_from_access_token returns credentials on existing token" do
    user = insert(:user)
    insert(:account, user: user)
    device = insert(:device, user: user)
    token = insert(:access_token, device: device)

    assert {%User{}, %Account{}, %Device{}, %AccessToken{}} =
             Plasma.Services.Auth.get_auth_data_from_access_token_value(token.value)
  end

  test "delete device" do
    user = insert(:user)
    insert(:account, user: user)
    device = insert(:device, user: user)

    assert {:ok, %Device{}} =
             Plasma.Services.Auth.delete_device(user.mx_user_id, device.mx_device_id)
  end

  test "delete device with_access_token" do
    user = insert(:user)
    insert(:account, user: user)
    device = insert(:device, user: user)
    insert(:access_token, device: device)

    assert {:ok, %Device{}} =
             Plasma.Services.Auth.delete_device(user.mx_user_id, device.mx_device_id)
  end
end
