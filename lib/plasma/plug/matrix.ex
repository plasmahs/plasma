defmodule Plasma.Plug.Matrix do
  use Plug.Router
  use Plug.ErrorHandler

  plug(Plug.RequestId)
  plug(Plug.Logger, log: :debug)

  plug(Plug.Static, from: :plasma, at: "/public")

  plug(:match)
  plug(:dispatch, builder_opts())

  # this is just like
  # forward "/_matrix/client", to: Polyjuice.Server.Plug.Client
  # except we want to forward the opts
  match "/_matrix/client/*path" do
    Plug.forward(conn, path, Polyjuice.Server.Plug.Client, opts)
  end

  match "/_matrix/federation/*path" do
    Plug.forward(conn, path, Polyjuice.Server.Plug.RoomDirectory.Federation, opts)
  end

  match "/_matrix/key/*path" do
    Plug.forward(conn, path, Polyjuice.Server.Plug.Key, opts)
  end

  match ".well-known/matrix/*path" do
    Plug.forward(conn, path, Polyjuice.Server.Plug.WellKnown, opts)
  end

  get "/" do
    server = opts[:server]

    name =
      case Polyjuice.Server.Protocols.BaseClientServer.impl_for(server) do
        nil ->
          "this"

        _ ->
          server_name = Polyjuice.Server.Protocols.BaseClientServer.get_server_name(server)
          "the <code>#{server_name}</code>"
      end

    conn
    |> put_resp_content_type("text/html")
    |> send_resp(200, ~s"""
    <!doctype html>
    <html lang="en">
    <head><title>Welcome to this Matrix server</title></head>
    <body>
    <h1>Welcome to #{name} Matrix server</h1>
    <p>To use this server, use a Matrix client, such as one listed at
    <a href="https://matrix.org/clients/">https://matrix.org/clients/</a>.</p>
    </body>
    </html>
    """)
  end

  match _ do
    conn
    |> put_resp_content_type("text/plain")
    |> send_resp(404, "Nothing to see here.")
  end

  def handle_errors(
        conn,
        %{kind: :error, reason: %FunctionClauseError{function: :do_match}, stack: _stack}
      ) do
    # endpoint not implemented (we assume that an error on *.do_match is a
    # router plug error)
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(400, ~s({"errcode":"M_UNRECOGNIZED","error":"Unrecognized request"}))
  end

  def handle_errors(
        conn,
        %{kind: _, reason: %{plug_status: status, matrix_error: err}, stack: _stack}
      ) do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(status, Jason.encode!(err))
  end

  def handle_errors(conn, %{kind: _kind, reason: _reason, stack: _stack} = err) do
    IO.puts(inspect(err))

    conn
    |> put_resp_content_type("application/json")
    |> send_resp(500, ~s({"errcode":"M_UNKNOWN","error":"Unknown error"}))
  end
end
