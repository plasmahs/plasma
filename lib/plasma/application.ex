# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false
  require Logger
  use Application

  @version Mix.Project.config()[:version]

  @impl true
  def start(_type, _args) do
    backend_conf = Application.fetch_env!(:plasma, Plasma.Backend)

    {endpoint_child_spec, endpoint_info} = endpoint_child_spec_from_conf()

    children = [
      # Start the Ecto repository
      Plasma.Repo,
      {Cachex, name: :auth_token_cache},
      {Oban, oban_config()},
      # Start Backend
      {Plasma.Backend, backend_conf},
      endpoint_child_spec,
      # Start the Telemetry supervisor
      Plasma.Telemetry
    ]

    # ++ endpoints

    Logger.info(info(endpoint_info))

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Plasma.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp info(endpoint_info) do
    cowboy = "cowboy #{Application.spec(:cowboy)[:vsn]}"
    # at #{bound_address(scheme, ref)}"
    "Running Plasma #{@version} with #{cowboy} at #{endpoint_info}"
  end

  defp endpoint_child_spec_from_conf() do
    with {:ok, endpoint_conf} <- Application.fetch_env(:plasma, :endpoint) do
      scheme = Keyword.keys(endpoint_conf) |> List.first()
      opts = Keyword.get(endpoint_conf, scheme)

      {{Plug.Cowboy,
        scheme: scheme, plug: {Plasma.Plug.Matrix, server: %Plasma.Backend{}}, options: opts},
       ~s/#{Keyword.get(opts, :ip, IP.localhost()) |> IP.to_string()}:#{Keyword.get(opts, :port, "4000")} (#{scheme})/}
    else
      _ ->
        Logger.warn("No endpoint configured !!")
        {}
    end
  end

  defp oban_config do
    Application.fetch_env!(:plasma, Oban)
  end
end
