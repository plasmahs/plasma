# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Backend do
  defstruct []

  alias Plasma.Backend
  alias Plasma.Errors.InternalError
  require Logger

  use Supervisor

  def start_link(conf) do
    Logger.debug("Starting Matrix backend with conf:")
    Logger.debug("#{inspect(conf)}")
    Supervisor.start_link(__MODULE__, conf, name: __MODULE__)
  end

  def child_spec(opts) do
    opts
    |> super()
    |> Supervisor.child_spec(id: __MODULE__)
  end

  @impl true
  def init(conf) do
    children = [
      {Plasma.Backend.UserBackend, conf},
      {Plasma.Backend.BaseClientBackend, conf},
      {Plasma.Backend.PubSubBackend, conf},
      {Plasma.Backend.DeviceKeyBackend, conf},
      {Plasma.Backend.AccountDataBackend, conf},
      {Plasma.Backend.PushRuleBackend, conf}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end

  def call_delegate(delegate, args, timeout \\ 5000) do
    Logger.debug("Backend called with args '#{inspect(args)}'")

    case GenServer.whereis(delegate) do
      nil ->
        Logger.critical(
          "Backend could not find GenServer delegate with name '#{delegate}' (A001)"
        )

        raise InternalError,
          matrix_error:
            PlasmaInternalError.new("Internal error occured, please report errorcode : A001")

      pid ->
        case GenServer.call(pid, args, timeout) do
          {:ok, response} ->
            response

          {:error, status, matrix_error} ->
            Logger.debug("Error reported from backend delegate: #{inspect(matrix_error)}")

            raise Polyjuice.Server.Plug.MatrixError,
              message: "Error reported from backend delegate",
              matrix_error: matrix_error,
              plug_status: status
        end
    end
  end

  defimpl Polyjuice.Server.Protocols.BaseClientServer do
    def get_server_name(%Backend{}) do
      Backend.call_delegate(
        Plasma.Backend.BaseClientBackend,
        {Polyjuice.Server.Protocols.BaseClientServer, :get_server_name, {}}
      )
    end

    def user_from_access_token(%Backend{}, access_token)
        when is_binary(access_token) do
      Backend.call_delegate(
        Plasma.Backend.BaseClientBackend,
        {Polyjuice.Server.Protocols.BaseClientServer, :user_from_access_token, {access_token}}
      )
    end

    def user_in_appservice_namespace?(%Backend{}, appservice_user_id, user_id)
        when is_binary(appservice_user_id) and is_binary(user_id) do
      Backend.call_delegate(
        Plasma.Backend.UserBackend,
        {Polyjuice.Server.Protocols.BaseClientServer, :user_in_appservice_namespace?,
         {appservice_user_id, user_id}}
      )
    end
  end

  defimpl Polyjuice.Server.Protocols.BaseFederation do
    def get_server_name(%Backend{}) do
      Backend.call_delegate(
        Plasma.Backend.FederationBackend,
        {Polyjuice.Server.Protocols.BaseFederation, :get_server_name, {}}
      )
    end

    def get_keys(%Backend{}) do
      Backend.call_delegate(
        Plasma.Backend.FederationBackend,
        {Polyjuice.Server.Protocols.BaseFederation, :get_keys, []}
      )
    end

    def get_old_keys(%Backend{}) do
      Backend.call_delegate(
        Plasma.Backend.FederationBackend,
        {Polyjuice.Server.Protocols.BaseFederation, :get_old_keys, {}}
      )
    end

    def get_cached_remote_keys(%Backend{}, origin, key_ids)
        when is_binary(origin) and (is_list(key_ids) or key_ids == nil) do
      Backend.call_delegate(
        Plasma.Backend.FederationBackend,
        {Polyjuice.Server.Protocols.BaseFederation, :get_cached_remote_keys, {origin, key_ids}}
      )
    end

    def cache_remote_keys(%Backend{}, origin, keys)
        when is_binary(origin) and is_map(keys) do
      Backend.call_delegate(
        Plasma.Backend.FederationBackend,
        {Polyjuice.Server.Protocols.BaseFederation, :cache_remote_keys, {origin, keys}}
      )
    end
  end

  defimpl Polyjuice.Server.Protocols.User do
    def login_flows(%Backend{}) do
      Backend.call_delegate(
        Plasma.Backend.UserBackend,
        {Polyjuice.Server.Protocols.User, :login_flows, {}}
      )
    end

    def log_in(%Backend{}, credentials, opts)
        when is_tuple(credentials) and is_list(opts) do
      Backend.call_delegate(
        Plasma.Backend.UserBackend,
        {Polyjuice.Server.Protocols.User, :log_in, {credentials, opts}}
      )
    end

    def log_out(%Backend{}, user_id, device_id)
        when is_binary(user_id) and is_binary(device_id) do
      Backend.call_delegate(
        Plasma.Backend.UserBackend,
        {Polyjuice.Server.Protocols.User, :log_out, {user_id, device_id}}
      )
    end

    def get_user_devices(%Backend{}, user_id) when is_binary(user_id) do
      Backend.call_delegate(
        Plasma.Backend.UserBackend,
        {Polyjuice.Server.Protocols.User, :get_user_devices, {user_id}}
      )
    end

    def register(%Backend{}, kind, username, password, log_in, opts)
        when (kind == :user or kind == :guest) and is_binary(username) and
               is_binary(password) and is_boolean(log_in) and is_list(opts) do
      Backend.call_delegate(
        Plasma.Backend.UserBackend,
        {Polyjuice.Server.Protocols.User, :register, {kind, username, password, log_in, opts}}
      )
    end

    def change_password(%Backend{}, user_id, password)
        when is_binary(user_id) and is_binary(password) do
      Backend.call_delegate(
        Plasma.Backend.UserBackend,
        {Polyjuice.Server.Protocols.User, :change_password, {user_id, password}}
      )
    end

    def deactivate(%Backend{}, user_id) when is_binary(user_id) do
      Backend.call_delegate(
        Plasma.Backend.UserBackend,
        {Polyjuice.Server.Protocols.User, :deactivate, {user_id}}
      )
    end

    def user_interactive_auth(%Backend{}, access_token, type, endpoint, auth, request)
        when is_binary(access_token) and is_atom(type) and is_binary(endpoint) and
               (is_map(auth) or auth == nil) and is_map(request) do
      Backend.call_delegate(
        Plasma.Backend.UserBackend,
        {Polyjuice.Server.Protocols.User, :user_interactive_auth,
         {access_token, type, endpoint, auth, request}}
      )
    end
  end

  defimpl Polyjuice.Server.Protocols.AccountData do
    def set(%Backend{}, user_id, room_id, type, data) do
      Backend.call_delegate(
        Plasma.Backend.AccountDataBackend,
        {Polyjuice.Server.Protocols.AccountData, :set, {user_id, room_id, type, data}}
      )
    end

    def get(%Backend{}, user_id, room_id, type) do
      Backend.call_delegate(
        Plasma.Backend.AccountDataBackend,
        {Polyjuice.Server.Protocols.AccountData, :get, {user_id, room_id, type}}
      )
    end
  end

  defimpl Polyjuice.Server.Protocols.DeviceKey do
    def set_device_keys(%Backend{}, user_id, device_id, device_keys) do
      Backend.call_delegate(
        Plasma.Backend.DeviceKeyBackend,
        {Polyjuice.Server.Protocols.DeviceKeys, :set_device_keys,
         {user_id, device_id, device_keys}}
      )
    end

    def add_one_time_keys(%Backend{}, user_id, device_id, one_time_keys) do
      Backend.call_delegate(
        Plasma.Backend.DeviceKeyBackend,
        {Polyjuice.Server.Protocols.DeviceKeys, :add_one_time_keys,
         {user_id, device_id, one_time_keys}}
      )
    end

    def set_cross_signing_keys(%Backend{}, user_id, device_keys) do
      Backend.call_delegate(
        Plasma.Backend.DeviceKeyBackend,
        {Polyjuice.Server.Protocols.DeviceKeys, :set_cross_signing_keys, {user_id, device_keys}}
      )
    end

    def add_signatures(%Backend{}, user_id, signatures) do
      Backend.call_delegate(
        Plasma.Backend.DeviceKeyBackend,
        {Polyjuice.Server.Protocols.DeviceKeys, :add_signatures, {user_id, signatures}}
      )
    end

    def claim_key(%Backend{}, one_time_keys) do
      Backend.call_delegate(
        Plasma.Backend.DeviceKeyBackend,
        {Polyjuice.Server.Protocols.DeviceKeys, :claim_key, {one_time_keys}}
      )
    end

    def query_keys(%Backend{}, user_id, device_keys, token) do
      Backend.call_delegate(
        Plasma.Backend.DeviceKeyBackend,
        {Polyjuice.Server.Protocols.DeviceKeys, :query_keys, {user_id, device_keys, token}}
      )
    end

    def get_changes(%Backend{}, user_id, from, to) do
      Backend.call_delegate(
        Plasma.Backend.DeviceKeyBackend,
        {Polyjuice.Server.Protocols.DeviceKeys, :get_changes, {user_id, from, to}}
      )
    end

    def key_counts(%Backend{}, user_id, device_id) do
      Backend.call_delegate(
        Plasma.Backend.DeviceKeyBackend,
        {Polyjuice.Server.Protocols.DeviceKeys, :key_counts, {user_id, device_id}}
      )
    end
  end

  defimpl Polyjuice.Server.Protocols.PushRule do
    def get_push_rules(%Backend{}, user_id, scope \\ nil) do
      Backend.call_delegate(
        Plasma.Backend.PushRuleBackend,
        {Polyjuice.Server.Protocols.PushRule, :get_push_rule, {user_id, scope, nil, nil}}
      )
    end

    def get_push_rule(%Backend{}, user_id, scope, kind, rule_id) do
      Backend.call_delegate(
        Plasma.Backend.PushRuleBackend,
        {Polyjuice.Server.Protocols.PushRule, :get_push_rule, {user_id, scope, kind, rule_id}}
      )
    end
  end

  defimpl Polyjuice.Server.Protocols.Room do
    def create(%Backend{}, state_events) when is_list(state_events) do
      Backend.call_delegate(
        Plasma.Backend.RoomBackend,
        {Polyjuice.Server.Protocols.Room, :create, {state_events}}
      )
    end

    def join(%Backend{}, user_id, room_id, opts)
        when is_binary(user_id) and is_binary(room_id) and is_list(opts) do
      Backend.call_delegate(
        Plasma.Backend.RoomBackend,
        {Polyjuice.Server.Protocols.Room, :join, {user_id, room_id, opts}}
      )
    end

    def send_event(%Backend{}, user_id, room_id, event_type, state_key, content)
        when is_binary(user_id) and is_binary(room_id) and is_binary(event_type) and
               (is_binary(state_key) or state_key == nil) and is_map(content) do
      Backend.call_delegate(
        Plasma.Backend.RoomBackend,
        {Polyjuice.Server.Protocols.Room, :send_event,
         {user_id, room_id, event_type, state_key, content}}
      )
    end

    def get_state(%Backend{}, user_id, event_id, event_type, state_key)
        when is_binary(user_id) and is_binary(event_id) and
               (is_binary(event_type) or event_type == nil) and
               (is_binary(state_key) or state_key == nil) do
      Backend.call_delegate(
        Plasma.Backend.RoomBackend,
        {Polyjuice.Server.Protocols.Room, :get_state, {user_id, event_id, event_type, state_key}}
      )
    end

    def get_messages(%Backend{}, user_id, room_id, token, direction, opts)
        when is_binary(user_id) and is_binary(room_id) and is_binary(token) and
               (direction == :forward or direction == :backward) and is_list(opts) do
      Backend.call_delegate(
        Plasma.Backend.RoomBackend,
        {Polyjuice.Server.Protocols.Room, :get_messages,
         {user_id, room_id, token, direction, opts}}
      )
    end

    def get_event(%Backend{}, user_id, room_id, event_id)
        when is_binary(user_id) and is_binary(room_id) and is_binary(event_id) do
      Backend.call_delegate(
        Plasma.Backend.RoomBackend,
        {Polyjuice.Server.Protocols.Room, :get_event, {user_id, room_id, event_id}}
      )
    end

    def forget(%Backend{}, user_id, room_id)
        when is_binary(user_id) and is_binary(room_id) do
      Backend.call_delegate(
        Plasma.Backend.RoomBackend,
        {Polyjuice.Server.Protocols.Room, :forget, {user_id, room_id}}
      )
    end

    def kick(%Backend{}, user_id, room_id, target_user_id, reason)
        when is_binary(user_id) and is_binary(room_id) and is_binary(target_user_id) and
               is_binary(reason) do
      Backend.call_delegate(
        Plasma.Backend.RoomBackend,
        {Polyjuice.Server.Protocols.Room, :kick, {user_id, room_id, target_user_id, reason}}
      )
    end

    def invite(%Backend{}, user_id, room_id, target_user_id, reason)
        when is_binary(user_id) and is_binary(room_id) and is_binary(target_user_id) and
               is_binary(reason) do
      Backend.call_delegate(
        Plasma.Backend.RoomBackend,
        {Polyjuice.Server.Protocols.Room, :invite, {user_id, room_id, target_user_id, reason}}
      )
    end

    def unban(%Backend{}, user_id, room_id, target_user_id, reason)
        when is_binary(user_id) and is_binary(room_id) and is_binary(target_user_id) and
               is_binary(reason) do
      Backend.call_delegate(
        Plasma.Backend.RoomBackend,
        {Polyjuice.Server.Protocols.Room, :unban, {user_id, room_id, target_user_id, reason}}
      )
    end
  end

  defimpl Polyjuice.Server.Protocols.PubSub do
    def publish(%Backend{}, data, queues) when is_list(queues) do
      Backend.call_delegate(
        Plasma.Backend.PubSubBackend,
        {Polyjuice.Server.Protocols.PubSub, :publish, {data, queues}}
      )
    end

    def subscribe(%Backend{}, subscriber, queues) when is_list(queues) do
      Backend.call_delegate(
        Plasma.Backend.PubSubBackend,
        {Polyjuice.Server.Protocols.PubSub, :subscribe, {subscriber, queues}}
      )
    end

    def unsubscribe(%Backend{}, subscriber, queues) when is_list(queues) do
      Backend.call_delegate(
        Plasma.Backend.PubSubBackend,
        {Polyjuice.Server.Protocols.PubSub, :unsubscribe, {subscriber, queues}}
      )
    end

    def await_sync(%Backend{}, user_id, device_id, sync_token, timeout, filter)
        when is_binary(user_id) and is_binary(device_id) and is_integer(timeout) and
               timeout >= 0 and is_map(filter) do
      Backend.call_delegate(
        Plasma.Backend.PubSubBackend,
        {Polyjuice.Server.Protocols.PubSub, :await_sync,
         {user_id, device_id, sync_token, timeout, filter}},
        timeout + 10000
      )
    end

    def get_sync_data_since(%Backend{}, user_id, device_id, sync_token, filter)
        when is_binary(user_id) and is_binary(device_id) and
               (is_binary(sync_token) or sync_token == nil) and is_map(filter) do
      Backend.call_delegate(
        Plasma.Backend.PubSubBackend,
        {Polyjuice.Server.Protocols.PubSub, :get_sync_data_since,
         {user_id, device_id, sync_token, filter}}
      )
    end
  end
end
