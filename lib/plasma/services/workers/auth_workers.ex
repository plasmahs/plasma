defmodule Plasma.Services.Workers.AuthWorkers do
  use Oban.Worker, queue: :default
  alias Plasma.Repo.AccessTokens
  require Logger

  @impl Oban.Worker
  def perform(%Oban.Job{
        args: %{
          "command" => "last_seen_access_token",
          "access_token_id" => access_token_id,
          "last_seen" => last_seen
        }
      }) do
    Logger.debug("Update AccessToken(id=#{access_token_id}) last seen")

    case AccessTokens.get_by_id(access_token_id) do
      nil ->
        Logger.warn("No access_token found with AccessTokens(id=#{access_token_id})")

      access_token ->
        res = AccessTokens.update_access_token(access_token, %{last_seen: last_seen})

        case res do
          {:error, changeset} ->
            Logger.warn(
              "Failed to update AccessToken(id=#{access_token_id}): #{Plasma.Repo.Utils.changeset_error_to_string(changeset)}"
            )

          {:ok, struct} ->
            Logger.debug(
              "AccessToken(id=#{access_token_id}) last seen updated to #{struct.last_seen}"
            )
        end
    end

    :ok
  end
end
