# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Services.E2e do
  alias Plasma.Repo.Devices
  alias Plasma.Repo.DeviceKeys

  @doc """
  Add a Device key to a device given by its user ID and device ID
  """
  @spec add_device_key(mx_user_id :: String.t(), mx_device_id :: String.t(), map()) ::
          {:error, :device_not_found}
          | {:error, Ecto.Changeset.t()}
          | {:ok, Plasma.Repo.Model.DeviceKey.t()}
  def add_device_key(mx_user_id, mx_device_id, device_keys) do
    case Devices.get_by_mx_user_id_mx_device_id(mx_user_id, mx_device_id) do
      nil -> {:error, :device_not_found}
      device -> DeviceKeys.insert_device_key(device, %{key_json: device_keys})
    end
  end
end
