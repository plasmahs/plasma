# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Services.Auth do
  alias Plasma.Repo.Model.Device
  alias Plasma.Repo.Model.Account
  alias Plasma.Repo.Model.User
  alias Plasma.Repo.Model.AccessToken
  alias Plasma.Repo.Devices
  alias Plasma.Repo.AccessTokens
  alias Plasma.Repo.Accounts

  @access_token_length Application.get_env(:plasma, :access_token_length)

  @doc """
  Return authentication data information associated with en authentication token

  Returns:
   - `{%User{}, %Account{}, %Device{}`} found for the access token
   - `nil` if the access_token is not found
  """
  @spec get_auth_data_from_access_token_value(access_token :: String.t()) ::
          {User.t(), Account.t(), Device.t(), AccessToken.t()} | nil
  def get_auth_data_from_access_token_value(access_token) do
    {_, res} =
      Cachex.fetch(:auth_token_cache, access_token, fn key ->
        {:commit, AccessTokens.get_by_access_token_with_account_user_device(key)}
      end)

    res
  end

  @doc """
  get account from repo with the given maxtrix user_id and check password
  Account is preloaded with user and devices
  """
  @spec get_account_check_password(String.t(), String.t()) ::
          {:ok, Account.t()}
          | {:error, :login_failed}
          | {:error, :account_not_found}
          | {:error, :wrong_password}
  def get_account_check_password(user_id, password) do
    with account when not is_nil(account) <-
           Accounts.get_by_mx_user_id_with_devices_and_user(user_id) do
      case Argon2.verify_pass(password, account.password_hash) do
        true -> {:ok, account}
        false -> {:error, :wrong_password}
      end
    else
      nil -> {:error, :account_not_found}
    end
  end

  @doc """
  Add a device to an axisting account
  Given `map` is expected do contain new Device properties
  """
  @spec add_device(User.t(), map) ::
          {:ok, Device.t()} | {:error, Changeset.t()}
  def add_device(%User{} = user, initial_display_name) do
    Devices.insert_for_user(user, %{
      initial_display_name: initial_display_name,
      last_seen: DateTime.utc_now()
    })
  end

  @spec delete_device(String.t(), String.t()) :: {:ok, Device.t()} | {:error, Ecto.Changeset.t()}
  def delete_device(mx_user_id, mx_device_id) do
    with device when not is_nil(device) <-
           Devices.get_by_mx_user_id_mx_device_id(mx_user_id, mx_device_id),
         access_token <- AccessTokens.get_by_device(device) do
      if(not is_nil(access_token)) do
        # One access_token found for the device. Delete it from DB and cache
        case AccessTokens.delete(access_token) do
          {:ok, access_token} -> Cachex.del(:auth_token_cache, access_token.value)
          {:error, changeset} -> Cachex.del(:auth_token_cache, changeset.changes.value)
        end
      end

      Devices.delete_device(device)
    end
  end

  defp generate_access_token() do
    Polyjuice.Util.Randomizer.randomize(@access_token_length)
  end

  @spec create_access_token(Device.t()) ::
          {:ok, AccessToken.t()} | {:error, Ecto.Changeset.t()}
  def create_access_token(%Device{} = device) do
    AccessTokens.insert_access_token(device, %{value: generate_access_token()})
  end

  def seen(%AccessToken{} = access_token, last_seen) do
    %{command: "last_seen_access_token", access_token_id: access_token.id, last_seen: last_seen}
    |> Plasma.Services.Workers.AuthWorkers.new()
    |> Oban.insert()
  end

  def get_user_devices(mx_device_id) do
    Devices.get_by_mx_user_id(mx_device_id)
  end
end
