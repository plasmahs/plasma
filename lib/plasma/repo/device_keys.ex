# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Repo.DeviceKeys do
  import Ecto.Query
  alias Plasma.Repo
  alias Plasma.Repo.Model.DeviceKey
  alias Plasma.Repo.Model.Device
  alias Plasma.Repo.Model.OneTimeDeviceKey
  alias Plasma.Repo.Model.User

  @spec insert_device_key(device :: Device.t(), map) ::
          {:ok, DeviceKey.t()} | {:error, Ecto.Changeset.t()}
  def insert_device_key(device, attrs \\ %{}) do
    Ecto.build_assoc(device, :device_keys)
    |> DeviceKey.create_changeset(attrs)
    |> Repo.insert()
  end

  @spec insert_one_time_device_key(device :: Device.t(), map) ::
          {:ok, OneTimeDeviceKey.t()} | {:error, Ecto.Changeset.t()}
  def insert_one_time_device_key(device, attrs \\ %{}) do
    Ecto.build_assoc(device, :one_time_device_keys)
    |> OneTimeDeviceKey.create_changeset(attrs)
    |> Repo.insert()
  end

  @spec get_one_time_keys(mx_user_id :: String.t(), mx_device_id :: String.t()) :: [
          OneTimeDeviceKey
        ]
  def get_one_time_keys(mx_user_id, mx_device_id) do
    from(u in User,
      join: d in assoc(u, :devices),
      join: k in assoc(d, :one_time_device_keys),
      where: u.mx_user_id == ^mx_user_id and d.mx_device_id == ^mx_device_id,
      select: k
    )
    |> Repo.all()
  end

  @spec get_by_mx_user_id_device_id(mx_user_id :: String.t(), mx_device_ids :: [String.t()]) :: [
          {Device.t(), {DeviceKey.t()}}
        ]
  def get_by_mx_user_id_device_id(mx_user_id, mx_device_ids \\ []) do
    query =
      User
      |> join(:inner, [u], d in assoc(u, :devices))
      |> join(:inner, [_, d], k in assoc(d, :device_keys))
      |> where([u, _, _], u.mx_user_id == ^mx_user_id)

    query =
      case mx_device_ids do
        [] ->
          query

        _ ->
          query |> where([_u, d, _k], d.mx_device_id in ^mx_device_ids)
      end

    Repo.all(query |> select([u, d, k], {d, k}))
  end
end
