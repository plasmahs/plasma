# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Repo.AccessTokens do
  import Ecto.Query
  alias Plasma.Repo
  alias Plasma.Repo.Model.AccessToken

  @doc """
  Get device, account and user informations from a given access token
  """
  @spec get_by_access_token_with_account_user_device(access_token :: String.t()) ::
          {User.t(), Account.t(), Device.t(), AccessToken.t()} | nil
  def get_by_access_token_with_account_user_device(access_token) do
    query =
      from(at in AccessToken,
        join: d in assoc(at, :device),
        join: u in assoc(d, :user),
        join: a in assoc(u, :account),
        where: at.value == ^access_token,
        select: {u, a, d, at}
      )

    Repo.one(query)
  end

  @doc """
  Add an access token to an existing account and device
  """
  @spec insert_access_token(device :: Device.t(), map) ::
          {:ok, AccessToken.t()} | {:error, Ecto.Changeset.t()}
  def insert_access_token(device, attrs \\ %{}) do
    AccessToken.create_changeset(%AccessToken{}, device, attrs) |> Repo.insert()
  end

  @spec delete(access_token :: AccessToken.t()) ::
          {:ok, AccessToken.t()} | {:error, Ecto.Changeset.t()}
  def delete(access_token) do
    access_token |> Repo.delete()
  end

  @spec get_by_device(Device.t()) :: AccessToken.t() | nil
  def get_by_device(device) do
    query =
      from(at in AccessToken,
        where: at.device_id == ^device.id
      )
      |> Repo.one()
  end

  @spec get_by_id(access_token_id :: String.t()) :: Device.t() | nil
  def get_by_id(access_token_id) do
    AccessToken |> Repo.get(access_token_id)
  end

  @doc """
  Update an existing access_token in database
  """
  @spec update_access_token(access_token :: AccessToken.t(), attrs :: map) ::
          {:ok, AccessToken.t()} | {:error, Ecto.Changeset.t()}
  def update_access_token(access_token, attrs \\ %{}) do
    AccessToken.update_changeset(access_token, attrs)
    |> Repo.update()
  end
end
