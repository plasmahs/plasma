# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Repo.Accounts do
  import Ecto.Query
  alias Plasma.Repo
  alias Plasma.Repo.Model.Account

  @doc """
  Get account from Matrix user ID
  Account is returned with User and Devices loaded
  """
  @spec get_by_mx_user_id_with_devices_and_user(mx_user_id :: String.t()) :: Account.t() | nil
  def get_by_mx_user_id_with_devices_and_user(mx_user_id) do
    query =
      from(a in Account,
        join: u in assoc(a, :user),
        left_join: d in assoc(u, :devices),
        where: u.mx_user_id == ^mx_user_id,
        preload: [user: {u, devices: d}]
      )

    Repo.one(query)
  end
end
