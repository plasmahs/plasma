# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Repo.Model.User do
  use Plasma.Schema
  alias Plasma.Repo.Model.Account
  alias Plasma.Repo.Model.Device

  @type t :: %__MODULE__{
          mx_user_id: String.t(),
          display_name: String.t(),
          avatar_url: String.t(),
          account: String.t(),
          devices: [Device.t()]
        }

  schema "users" do
    field(:mx_user_id, :string)
    field(:display_name, :string)
    field(:avatar_url, :string)
    has_one(:account, Account)
    has_many(:devices, Device)
    timestamps()
  end

  def changeset(user, attrs \\ %{}) do
    user
    |> cast(attrs, [:mx_user_id, :display_name, :avatar_url])
    |> validate_required([:mx_user_id])
    |> validate_mx_user_id()
  end

  # Validate user_id format
  defp validate_mx_user_id(%Ecto.Changeset{valid?: true} = changeset) do
    case changeset do
      %Ecto.Changeset{changes: %{mx_user_id: user_id}} ->
        if Polyjuice.Util.Identifiers.V1.UserIdentifier.valid?(user_id) do
          changeset
        else
          Ecto.Changeset.add_error(changeset, :mx_user_id, "invalid_mx_user_id")
        end

      _ ->
        changeset
    end
  end

  defp validate_mx_user_id(changeset), do: changeset

  def create_changeset(user, attrs \\ %{}) do
    changeset(user, attrs)
    |> unique_constraint(:mx_user_id, message: "mx_user_id_already_exists")
  end
end
