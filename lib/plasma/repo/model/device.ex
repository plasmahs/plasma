# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Repo.Model.Device do
  use Plasma.Schema
  alias Plasma.Repo.Model.User
  alias Plasma.Repo.Model.DeviceKey
  alias Plasma.Repo.Model.OneTimeDeviceKey

  @type t :: %__MODULE__{
          mx_device_id: String.t(),
          display_name: String.t(),
          user: User.t()
        }

  schema "devices" do
    field(:mx_device_id, :string)
    field(:display_name, :string)
    belongs_to(:user, User)
    has_many(:device_keys, DeviceKey)
    has_many(:one_time_device_keys, OneTimeDeviceKey)
    timestamps()
  end

  def changeset(device, attrs \\ %{}) do
    device
    |> cast(attrs, [:mx_device_id, :display_name])
    |> validate_required([:mx_device_id])
    |> assoc_constraint(:user)
  end

  def create_changeset(device, attrs \\ %{}) do
    device
    |> change()
    |> put_mx_device_id()
    |> changeset(attrs)
  end

  defp put_mx_device_id(%Ecto.Changeset{valid?: true, changes: changes} = changeset) do
    case changes do
      %{mx_device_id: mx_device_id} when not is_nil(mx_device_id) -> changeset
      _ -> put_change(changeset, :mx_device_id, generate_device_id())
    end
  end

  defp generate_device_id() do
    Polyjuice.Util.Randomizer.randomize(10, :upcase)
  end

  def update_changeset(device, attrs \\ %{}) do
    changeset(device, attrs)
  end
end
