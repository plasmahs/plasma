# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Repo.Model.PushRule do
  use Plasma.Schema
  alias Plasma.Repo.Model.Account

  @type t :: %__MODULE__{
          scope: String.t(),
          kind: String.t(),
          priority: integer(),
          rule_id: String.t(),
          actions: map(),
          conditions: map(),
          enabled: boolean(),
          account: Account.t()
        }

  schema "push_rules" do
    field(:scope, :string)
    field(:kind, :string)
    field(:priority, :integer)
    field(:rule_id, :string)
    field(:actions, :map)
    field(:conditions, :map)
    field(:enabled, :boolean)
    belongs_to(:account, Account)
  end

  def changeset(push_rule, attrs \\ %{}) do
    push_rule
    |> cast(attrs, [:scope, :kind, :priority, :rule_id, :actions, :conditions, :enabled])
    |> assoc_constraint(:account)
  end

  def create_changeset(push_rule, attrs \\ %{}), do: changeset(push_rule, attrs)
end
