# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Repo.Model.OneTimeDeviceKey do
  use Plasma.Schema
  alias Plasma.Repo.Model.Device

  @type t :: %__MODULE__{
          device: Device.t(),
          key_json: map(),
          algorithm: String.t(),
          inserted_at: DateTime.t(),
          updated_at: DateTime.t()
        }

  schema "one_time_device_keys" do
    belongs_to(:device, Device)
    field(:key_json, :map)
    field(:algorithm, :string)
    timestamps()
  end

  def changeset(one_time_key, attrs \\ %{}) do
    one_time_key
    |> cast(attrs, [:key_json, :algorithm])
    |> assoc_constraint(:device)
  end

  def create_changeset(one_time_key, attrs \\ %{}) do
    one_time_key
    |> changeset(attrs)
  end
end
