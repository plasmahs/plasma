# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Repo.Model.AccountData do
  use Plasma.Schema
  alias Plasma.Repo.Model.Account
  alias Plasma.Repo.Model.AccountData
  alias Plasma.Repo.Model.Room

  @type t :: %__MODULE__{
          account: Account.t(),
          room: Room.t(),
          type: String.t(),
          content: String.t()
        }

  schema "account_datas" do
    belongs_to(:account, Account)
    belongs_to(:room, Room)
    field(:type, :string)
    field(:content, :map)
    timestamps()
  end

  def changeset(account_data, attrs \\ %{}) do
    account_data
    |> cast(attrs, [:type, :content])
    |> unique_constraint([:type, :account_id, :room_id],
      name: :account_data_type_account_id_room_id_unique_index
    )
  end

  def create_changeset(account_data, account, attrs) do
    account_data
    |> change()
    |> put_assoc(:account, account)
    |> changeset(attrs)
  end

  def create_changeset(account_data, account, room, attrs) do
    account_data
    |> change()
    |> put_assoc(:account, account)
    |> put_assoc(:room, room)
    |> changeset(attrs)
  end
end
