# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Repo.Model.AccessToken do
  use Plasma.Schema
  alias Plasma.Repo.Model.Device

  @type t :: %__MODULE__{
          value: String.t(),
          device: Device.t(),
          last_seen: DateTime.t(),
          soft_logout: boolean()
        }

  schema "access_tokens" do
    field(:value, :string)
    belongs_to(:device, Device)
    field(:last_seen, :utc_datetime)
    field(:soft_logout, :boolean)
    timestamps()
  end

  def changeset(token, attrs \\ %{}) do
    token
    |> cast(attrs, [:value, :last_seen, :soft_logout])
    |> assoc_constraint(:device)
  end

  def create_changeset(token, device, attrs \\ %{}) do
    token
    |> change()
    |> put_assoc(:device, device)
    |> changeset(attrs)
  end

  def update_changeset(token, attrs \\ %{}) do
    changeset(token, attrs)
  end
end
