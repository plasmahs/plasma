# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Repo.Devices do
  import Ecto.Query
  alias Plasma.Repo
  alias Plasma.Repo.Model.Device
  alias Plasma.Repo.Model.Account
  alias Plasma.Repo.Model.User

  @doc """
  Get device, account and user informations from a given access token
  """
  @spec get_by_access_token_with_account_user(access_token :: String.t()) ::
          {User.t(), Account.t(), Device.t()} | nil
  def get_by_access_token_with_account_user(access_token) do
    query =
      from(d in Device,
        join: u in assoc(d, :user),
        join: a in Account,
        on: u.id == a.user_id,
        where: d.access_token == ^access_token,
        select: {u, a, d}
      )

    Repo.one(query)
  end

  @spec get_by_mx_user_id_mx_device_id(mx_user_id :: String.t(), mx_device_id :: String.t()) ::
          Device.t() | nil
  def get_by_mx_user_id_mx_device_id(mx_user_id, mx_device_id) do
    query =
      from(u in User,
        join: d in assoc(u, :devices),
        where: u.mx_user_id == ^mx_user_id and d.mx_device_id == ^mx_device_id,
        select: d
      )

    Repo.one(query)
  end

  @doc """
  Get all devices known for a given Matrix user ID
  """
  @spec get_by_mx_user_id(mx_user_id :: String.t()) :: [Device.t()]
  def get_by_mx_user_id(mx_user_id) do
    from(u in User,
      join: d in assoc(u, :devices),
      where: u.mx_user_id == ^mx_user_id,
      select: d
    )
    |> Repo.all()
  end

  @doc """
  Insert a device and associates it to an existing user
  """
  @spec insert_for_user(user :: User.t(), map) ::
          {:ok, Device.t()} | {:error, Ecto.Changeset.t()}
  def insert_for_user(user, attrs \\ %{}) do
    Ecto.build_assoc(user, :devices)
    |> Device.create_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Update an existing device in database
  """
  @spec update_device(device :: Device.t(), attrs :: map) ::
          {:ok, Device.t()} | {:error, Ecto.Changeset.t()}
  def update_device(device, attrs \\ %{}) do
    Device.update_changeset(device, attrs)
    |> Repo.update()
  end

  @spec delete_device(device :: Device.t()) :: {:ok, Device.t()} | {:error, Ecto.Changeset.t()}
  def delete_device(device) do
    device |> Repo.delete()
  end

  @spec get_by_id(device_id :: String.t()) :: Device.t() | nil
  def get_by_id(device_id) do
    Device |> Repo.get(device_id)
  end
end
