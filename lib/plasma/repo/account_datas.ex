# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Repo.AccountDatas do
  import Ecto.Query
  alias Plasma.Repo
  alias Plasma.Repo.Model.Account
  alias Plasma.Repo.Model.AccountData
  alias Plasma.Repo.Model.Room

  @spec insert_account_data(account :: Account.t(), type :: String.t(), data :: String.t()) ::
          {:ok, AccountData.t()} | {:error, Ecto.Changeset.t()}
  def insert_account_data(account, type, data) do
    AccountData.create_changeset(%AccountData{}, account, %{type: type, content: data})
    |> Repo.insert()
  end

  @spec insert_account_data(
          account :: Account.t(),
          room :: Room.t(),
          type :: String.t(),
          data :: String.t()
        ) ::
          {:ok, AccountData.t()} | {:error, Ecto.Changeset.t()}
  def insert_account_data(account, room, type, data) do
    AccountData.create_changeset(%AccountData{}, account, room, %{type: type, content: data})
    |> Repo.insert()
  end

  defp get_account_data_query(mx_user_id, type) do
    Account
    |> join(:inner, [a], u in assoc(a, :user))
    |> join(:inner, [a, _], d in assoc(a, :account_data))
    |> join(:left, [_, _, d], r in assoc(d, :room))
    |> where([_, _, d, _], d.type == ^type)
    |> where([_, u, _, _], u.mx_user_id == ^mx_user_id)
  end

  def get_account_data(mx_user_id, type) do
    get_account_data_query(mx_user_id, type)
    |> where([_, _, d, _], is_nil(d.room_id))
    |> select([_, _, d, _], d)
    |> Repo.one()
  end

  def get_room_account_data(mx_user_id, mx_room_id, type) do
    get_account_data_query(mx_user_id, type)
    |> where([_, _, _, r], r.mx_room_id == ^mx_room_id)
    |> select([_, _, d, _], d)
    |> Repo.one()
  end
end
