# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Repo.PushRules do
  import Ecto.Query
  alias Plasma.Repo
  alias Plasma.Repo.Model.PushRule

  defp get_push_rules_query(mx_user_id, scope) do
    PushRule
    |> join(:inner, [pr], a in assoc(pr, :account))
    |> join(:inner, [pr, a], u in assoc(a, :user))
    |> where([pr, a, u], u.mx_user_id == ^mx_user_id)
    |> where([pr, a, u], pr.scope == ^scope)
  end

  @spec get_push_rules(
          mx_user_id :: String.t(),
          scope :: String.t(),
          kind :: String.t(),
          rule_id :: String.t()
        ) :: [PushRule.t()]
  def get_push_rules(mx_user_id, scope, kind, rule_id) do
    query = get_push_rules_query(mx_user_id, scope)

    query =
      if not is_nil(kind) do
        query |> where([pr, a, u], pr.kind == ^kind)
      end

    query =
      if not is_nil(rule_id) do
        query |> where([pr, a, u], pr.rule_id == ^rule_id)
      end

    Repo.all(query |> select([pr, a, u], pr))
  end
end
