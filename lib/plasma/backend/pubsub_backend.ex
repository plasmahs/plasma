# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Backend.PubSubBackend do
  use GenServer
  require Logger
  alias Plasma.Errors.PlasmaNotImplementedError

  @impl true
  def init(conf) do
    {:ok, conf}
  end

  def start_link(conf) do
    GenServer.start_link(__MODULE__, conf, name: __MODULE__)
  end

  @impl true
  def handle_call({Polyjuice.Server.Protocols.BaseClientServer, :get_server_name, _}, _from, conf) do
    server_name = conf |> Keyword.get(:matrix) |> Keyword.get(:server_name)
    {:reply, server_name, conf}
  end

  @impl true
  def handle_call(args, _from, conf) do
    Logger.debug("Call to #{inspect(args)}")
    {:reply, {:ok, %{}}, conf}
  end

  def handle_call({protocol, fun, _}, _from, conf) do
    Logger.warn("Unhandled call from protocol {#{protocol}, #{fun}}")
    {:reply, {:error, 500, PlasmaNotImplementedError.new()}, conf}
  end
end
