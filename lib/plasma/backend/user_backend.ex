# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Backend.UserBackend do
  use GenServer
  require Logger
  alias Plasma.Services.Auth
  alias Polyjuice.Util.ClientAPIErrors

  @impl true
  def init(conf) do
    {:ok, conf}
  end

  def start_link(conf) do
    GenServer.start_link(__MODULE__, conf, name: __MODULE__)
  end

  @impl true
  def handle_call({Polyjuice.Server.Protocols.User, :login_flows, _}, _from, conf) do
    login_types = conf |> Keyword.get(:matrix) |> Keyword.get(:login_types)
    {:reply, {:ok, login_types}, conf}
  end

  @impl true
  def handle_call(
        {Polyjuice.Server.Protocols.User, :log_in, {{:password, mx_user_id, password}, opts}},
        _from,
        conf
      ) do
    Logger.debug(
      "Handler called with :log_in :password auth. mx_user_id='#{mx_user_id}', password='#{password}'"
    )

    case Auth.get_account_check_password(mx_user_id, password) do
      {:ok, account} ->
        if not account.activated do
          # Account has been de-activated
          {:reply, {:error, 403, ClientAPIErrors.MUserDeactivated.new()}, conf}
        else
          [device_id: mx_device_id, initial_device_display_name: initial_display_name] = opts

          rep =
            case Enum.find(account.user.devices, fn device ->
                   device.mx_device_id == mx_device_id
                 end) do
              nil ->
                with {:ok, new_device} <- Auth.add_device(account.user, initial_display_name),
                     {:ok, access_token} <- Auth.create_access_token(new_device) do
                  Auth.seen(access_token, DateTime.utc_now())

                  {:ok,
                   %{
                     access_token: access_token.value,
                     user_id: account.user.mx_user_id,
                     device_id: new_device.mx_device_id
                   }}
                else
                  {:error, changeset} ->
                    {:error, 500,
                     PlasmaInternalError.new(
                       Plasma.Repo.Utils.changeset_error_to_string(changeset)
                     )}
                end

              device ->
                case Auth.create_access_token(device) do
                  {:ok, access_token} ->
                    Auth.seen(access_token, DateTime.utc_now())

                    {:ok,
                     %{
                       access_token: access_token.value,
                       user_id: account.user.mx_user_id,
                       device_id: device.mx_device_id
                     }}

                  {:error, changeset} ->
                    {:error, 500,
                     PlasmaInternalError.new(
                       Plasma.Repo.Utils.changeset_error_to_string(changeset)
                     )}
                end
            end

          {:reply, {:ok, rep}, conf}
        end

      {:error, _} ->
        # No account found with the given matrix user ID or password don't match
        {:reply, {:error, 403, ClientAPIErrors.MForbidden.new()}, conf}
    end
  end

  @doc """
  Default log_in implementation fails with unsupported login type
  """
  @impl true
  def handle_call(
        {Polyjuice.Server.Protocols.User, :log_in, {{login_type, _mx_user_id, _password}, _opts}},
        _from,
        conf
      ) do
    {:reply,
     {:error, 400, ClientAPIErrors.MBadJson.new("Unsupported login type '#{login_type}'")}, conf}
  end

  @impl true
  def handle_call({Polyjuice.Server.Protocols.User, :log_out, {user_id, device_id}}, _from, conf) do
    case Auth.delete_device(user_id, device_id) do
      {:ok, _} ->
        {:reply, {:ok, nil}, conf}

      {:error, changeset} ->
        {:reply,
         {:error, 500,
          PlasmaInternalError.new(Plasma.Repo.Utils.changeset_error_to_string(changeset))}}
    end
  end

  @impl true
  def handle_call({Polyjuice.Server.Protocols.User, :get_user_devices, {user_id}}, _from, conf) do
    try do
      device_ids =
        Auth.get_user_devices(user_id) |> Enum.map(fn device -> device.mx_device_id end)

      {:reply, {:ok, device_ids}, conf}
    rescue
      Ecto.QueryError -> {:reply, {:error, 500, PlasmaInternalError.new()}}
    end
  end

  def handle_call({protocol, fun, _args}, _from, conf) do
    Logger.warn("UserBackend: Unhandled call received from protocol {#{protocol}, #{fun}}")
    {:reply, {:error, 500, PlasmaNotImplementedError.new()}, conf}
  end
end
