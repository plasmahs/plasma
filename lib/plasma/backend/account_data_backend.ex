# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Plasma.Backend.AccountDataBackend do
  use GenServer
  require Logger
  alias Polyjuice.Util.ClientAPIErrors
  alias Plasma.Repo.Accounts
  alias Plasma.Repo.AccountDatas
  alias Plasma.Repo.Rooms

  @impl true
  def init(conf) do
    {:ok, conf}
  end

  def start_link(conf) do
    GenServer.start_link(__MODULE__, conf, name: __MODULE__)
  end

  @impl true
  def handle_call(
        {Polyjuice.Server.Protocols.AccountData, :set, {user_id, room_id, type, data}},
        _from,
        conf
      ) do
    with account <- Accounts.get_by_mx_user_id_with_devices_and_user(user_id),
         room <- Rooms.get_by_mx_room_id(room_id) do
      ret =
        case [account, room] do
          [nil, _] ->
            # This shouldn't happen
            {:reply, {:error, 400, ClientAPIErrors.MBadJson.new("Uknown user '#{user_id}'")},
             conf}

          [account, nil] ->
            AccountDatas.insert_account_data(account, type, data)

          [account, room] ->
            AccountDatas.insert_account_data(account, room, type, data)
        end

      case ret do
        {:ok, _account_data} ->
          {:reply, {:ok, :ok}, conf}

        {:error, changeset} ->
          {:reply,
           {:error, 500,
            PlasmaInternalError.new(Plasma.Repo.Utils.changeset_error_to_string(changeset))}}
      end
    end
  end

  def handle_call(
        {Polyjuice.Server.Protocols.AccountData, :get, {user_id, room_id, type}},
        _from,
        conf
      ) do
    account_data =
      case room_id do
        nil ->
          AccountDatas.get_account_data(user_id, type)

        room_id ->
          AccountDatas.get_room_account_data(user_id, room_id, type)
      end

    {:reply, {:ok, {:ok, account_data.content}}, conf}
  end
end
