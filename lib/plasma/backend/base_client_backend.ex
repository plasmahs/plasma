# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
defmodule Plasma.Backend.BaseClientBackend do
  use GenServer
  require Logger
  alias Plasma.Services.Auth
  alias Plasma.Errors.PlasmaNotImplementedError

  @impl true
  def init(conf) do
    {:ok, conf}
  end

  def start_link(conf) do
    GenServer.start_link(__MODULE__, conf, name: __MODULE__)
  end

  @impl true
  def handle_call({Polyjuice.Server.Protocols.BaseClientServer, :get_server_name, _}, _from, conf) do
    server_name = conf |> Keyword.get(:matrix) |> Keyword.get(:server_name)
    {:reply, {:ok, server_name}, conf}
  end

  @impl true
  def handle_call(
        {Polyjuice.Server.Protocols.BaseClientServer, :user_from_access_token, {token_value}},
        _from,
        conf
      ) do
    case Auth.get_auth_data_from_access_token_value(token_value) do
      # TODO: auth_token doesn't belong to a user, we should check if it's an appservice
      nil ->
        {:reply, nil, conf}

      {user, account, device, access_token} ->
        case access_token.soft_logout do
          true ->
            {:reply,
             {:ok, {:soft_logout, user.mx_user_id, device.mx_device_id, account.kind == "guest"}},
             conf}

          _ ->
            {:reply,
             {:ok, {:user, user.mx_user_id, device.mx_device_id, account.kind == "guest"}}, conf}
        end
    end
  end

  def handle_call({protocol, fun, _}, _from, conf) do
    Logger.warn("Unhandled call from protocol {#{protocol}, #{fun}}")
    {:reply, {:error, 500, PlasmaNotImplementedError.new()}, conf}
  end
end
