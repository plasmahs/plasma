defmodule Plasma.Backend.DeviceKeyBackend do
  use GenServer
  require Logger
  alias Plasma.Repo.DeviceKeys
  alias Plasma.Repo.Devices
  alias Polyjuice.Util.ClientAPIErrors

  @impl true
  def init(conf) do
    {:ok, conf}
  end

  def start_link(conf) do
    GenServer.start_link(__MODULE__, conf, name: __MODULE__)
  end

  @impl true
  def handle_call(
        {Polyjuice.Server.Protocols.DeviceKeys, :set_device_keys,
         {user_id, device_id, device_keys}},
        _from,
        conf
      ) do
    Devices.get_by_mx_user_id_mx_device_id(user_id, device_id)
    |> DeviceKeys.insert_device_key(%{key_json: device_keys})

    {:reply, {:ok, nil}, conf}
  end

  @impl true
  def handle_call(
        {Polyjuice.Server.Protocols.DeviceKeys, :add_one_time_keys,
         {user_id, device_id, one_time_keys}},
        _from,
        conf
      ) do
    device = Devices.get_by_mx_user_id_mx_device_id(user_id, device_id)

    Enum.each(one_time_keys, fn {pkey, key} ->
      [algorithm, _] = String.split(pkey, ":", parts: 2)

      DeviceKeys.insert_one_time_device_key(device, %{
        algorithm: algorithm,
        key_json: %{pkey => key}
      })
    end)

    {:reply, {:ok, nil}, conf}
  end

  @impl true
  def handle_call(
        {Polyjuice.Server.Protocols.DeviceKeys, :key_counts, {user_id, device_id}},
        _from,
        conf
      ) do
    counts =
      DeviceKeys.get_one_time_keys(user_id, device_id)
      |> Enum.frequencies_by(fn one_key -> one_key.algorithm end)

    {:reply, {:ok, %{one_time_keys: counts}}, conf}
  end

  @impl true
  def handle_call(
        {Polyjuice.Server.Protocols.DeviceKeys, :query_keys, {_user_id, devices, token}},
        _from,
        conf
      ) do
    if not is_nil(token) do
      Logger.warn(
        "Unsupported token argument in {Polyjuice.Server.Protocols.DeviceKeys, :query_keys} calls"
      )
    end

    device_keys =
      Enum.map(devices, fn {user_id, devices} ->
        user_device_key =
          DeviceKeys.get_by_mx_user_id_device_id(user_id, devices)
          |> Enum.map(fn {device, key} -> {device.mx_device_id, key.key_json} end)
          |> Map.new()

        {user_id, user_device_key}
      end)
      |> Map.new()

    {:reply, {:ok, %{"device_keys" => device_keys}}, conf}
  end
end
