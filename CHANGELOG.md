# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased] - 2022-01-15

### Added

- implement `PUT /_matrix/client/v3/user/{userId}/account_data/{type}`
- implement `GET /_matrix/client/v3/user/{userId}/account_data/{type}`
- implement `PUT /_matrix/client/v3/user/{userId}/rooms/{roomId}/account_data/{type}`
- implement `GET /_matrix/client/v3/user/{userId}/rooms/{roomId}/account_data/{type}`
- implement `POST /_matrix/client/v3/keys/query` (`token` parameter is not supported)
- implement `POST /_matrix/client/v3/keys/upload`
- implement `POST /_matrix/client/r0/login`
- implement `POST /_matrix/client/r0/logout`
- implement `POST /_matrix/client/r0/logout/all`
