# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Plasma.Repo.insert!(%Plasma.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Plasma.Repo
alias Plasma.Repo.Model.User
alias Plasma.Repo.Model.Account

if Mix.env() == :dev do
  user = Repo.insert!(User.create_changeset(%User{mx_user_id: "@user1:localhost"}))
  Repo.insert!(Ecto.build_assoc(user, :account) |> Account.create_changeset(%{password: "password", activated: true}))
end
