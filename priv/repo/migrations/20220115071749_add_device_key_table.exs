defmodule Plasma.Repo.Migrations.AddDeviceKeyTable do
  use Ecto.Migration

  def change do
    create table(:device_keys) do
      add :device_id, references(:devices)
      add :key_json, :map
      timestamps()
    end
  end
end
