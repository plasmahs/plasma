defmodule Plasma.Repo.Migrations.AddRoomTable do
  use Ecto.Migration

  def change do
    create table(:rooms) do
      add :mx_room_id, :string, null: false
      timestamps()
    end
    create unique_index(:rooms, [:mx_room_id])

  end
end
