defmodule Plasma.Repo.Migrations.AddAccountTable do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :user_id, references(:users)
      add :password_hash, :text
      add :kind, :string
      add :activated, :boolean
      timestamps()
    end
  end
end
