defmodule Plasma.Repo.Migrations.AddOneTimeDeviceKeyTable do
  use Ecto.Migration

  def change do
    create table(:one_time_device_keys) do
      add :device_id, references(:devices)
      add :algorithm, :string, null: false
      add :key_json, :map
      timestamps()
    end
  end
end
