defmodule Plasma.Repo.Migrations.AddDeviceTable do
  use Ecto.Migration

  def change do
    create table(:devices) do
      add :mx_device_id, :text, null: false
      add :display_name, :text
      add :access_token, :text
      add :user_id, references(:users)
      add :last_seen, :utc_datetime
      add :soft_logout, :boolean
      timestamps()
    end
  end
end
