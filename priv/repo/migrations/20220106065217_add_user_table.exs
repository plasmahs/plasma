defmodule Plasma.Repo.Migrations.AddUserTable do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :mx_user_id, :text, null: false
      add :display_name, :text
      add :avatar_url, :text
      timestamps()
    end

    create unique_index(:users, [:mx_user_id])
  end
end
