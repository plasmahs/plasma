defmodule Plasma.Repo.Migrations.AddAccountDataTable do
  use Ecto.Migration

  def change do
    create table(:account_datas) do
      add :type, :string, null: false
      add :content, :map
      add :account_id, references(:accounts, on_delete: :delete_all)
      add :room_id, references(:rooms, on_delete: :delete_all)
      timestamps()
    end

    create unique_index("account_datas", [:account_id, :room_id, :type], name: :account_data_type_account_id_room_id_unique_index)
  end
end
