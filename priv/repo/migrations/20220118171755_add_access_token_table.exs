defmodule Plasma.Repo.Migrations.AddAccessTokenTable do
  use Ecto.Migration

  def change do
    create table(:access_tokens) do
      add :value, :text
      add :device_id, references(:devices, on_delete: :delete_all)
      add :last_seen, :utc_datetime
      add :soft_logout, :boolean
      timestamps()
    end

  end
end
