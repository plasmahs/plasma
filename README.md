# Plasma

Plasma is an open-source [Matrix](https://matrix.org/) server implementation.

[Matrix](https://matrix.org/) is an open standard for interoperable, decentralised, real-time communication over IP.
It can be used to power Instant Messaging, VoIP/WebRTC signalling, Internet of Things communication -
or anywhere you need a standard HTTP API for publishing and subscribing to data whilst tracking the
conversation history.

Plasma is written with [Elixir language](https://elixir-lang.org/). Elixir compiles to Erlang VM so Plasma can run on the [BEAM virtual machine](https://en.wikipedia.org/wiki/BEAM_(Erlang_virtual_machine)). Plasma relies on these technologies to provide a low latency server supporting high concurrency.

Last but not least, Plasma relies on [Polyjuice libraries](https://gitlab.com/polyjuice) especially the [Polyjuice server framework](https://gitlab.com/polyjuice/polyjuice_server).

**Tis project is still under heavy construction** Please see CHANGELOG for current status.

